# frozen_string_literal: true

require 'active_record/errors'
# Close shift action
class CloseShiftAction < AbstractAction

  def perform(params)
    shift_id = params['shift_id']
    begin
      shift = Shift.find(shift_id)
    rescue ActiveRecord::RecordNotFound
      return error_result("Смена с id #{shift_id} не найдена")
    end
    return error_result("Смена с id #{shift_id} уже закрыта") if shift.closed?

    update_result = shift.update_attributes(state: 'closed', endDate: DateTime.now)
    if update_result
      success_result
    else
      error_result("Произошла ошибка при обновлении смены с id #{shift_id}")
    end
  end

end
