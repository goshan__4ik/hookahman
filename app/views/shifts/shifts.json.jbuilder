json.array! @shifts do |shift|
  json.id shift.id
  json.startDate shift.startDate
  json.endDate shift.endDate
  json.state shift.state
end