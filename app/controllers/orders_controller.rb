# frozen_string_literal: true

require 'date'
# Shifts Controller
class OrdersController < UserActionController
  def orders
    shift = if params.key?('id')
              params['id']
            else
              Shift.find_by(employee: @current_user.id, state: 'active').id
            end
    @orders = Order.where('shift = ?', shift).order('id desc')
  end

  def create
    body = JSON.parse(request.raw_post)
    order_params = Hash['startDate', DateTime.now,
                        'table_id', body['table'],
                        'state', 'active',
                        'personsCount', body['personsCount'],
                        'shift', params[:id]]
    @order = Order.new(order_params)
    @order.save
    render status: :created
  end

  private

  CLOSE_ACTION = 'close'

  def action_params(action_name)
    unless action_name != CLOSE_ACTION
      # TODO
    end
    Hash['order_id', params['id']]
  end

  def possible_actions
    Hash[CLOSE_ACTION, CloseOrderAction.new]
  end

end
