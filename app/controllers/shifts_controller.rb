# frozen_string_literal: true

require 'date'
# Shifts Controller
class ShiftsController < UserActionController
  def shifts
    @shifts = if @current_user.role == 'admin'
                Shift.all.order('id DESC')
              else
                Shift.where(employee: @current_user.id).order('id DESC')
              end
  end

  def create
    if current_user_active_shift?
      error = Hash['description', 'У вас уже есть активная смена']
      render body: error.to_json, status: :bad_request
    else
      start_date = DateTime.now
      shift = Shift.new(startDate: start_date, state: 'active', employee: @current_user.id)
      shift.save
      render json: Hash['id', shift.id].to_json
    end
  end

  private

  CLOSE_ACTION = 'close'

  def action_params(action_name)
    unless action_name != CLOSE_ACTION
      # TODO
    end
    Hash['shift_id', params['id']]
  end

  def possible_actions
    Hash[CLOSE_ACTION, CloseShiftAction.new]
  end

  def current_user_active_shift?
    !Shift.where('employee = ? and state = ?', @current_user.id, 'active').empty?
  end
end
