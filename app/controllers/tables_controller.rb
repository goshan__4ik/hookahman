# frozen_string_literal: true

require 'date'
# Shifts Controller
class TablesController < UserActionController

  def get
    @tables = Table.all
  end

end
