# frozen_string_literal: true

# Shift
class Shift < ApplicationRecord

  CLOSED_STATE = 'closed'

  def closed?
    state == CLOSED_STATE
  end
end
