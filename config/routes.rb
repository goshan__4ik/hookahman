Rails.application.routes.draw do
  post 'authenticate', to: 'authentication#authenticate'

  get '/shifts', to: 'shifts#shifts'
  post '/shifts', to: 'shifts#create'
  post '/shifts/:id', to: 'shifts#update'

  get '/shifts/:id/orders', to: 'orders#orders'
  post '/shifts/:id/orders', to: 'orders#create'
  post '/orders/:id', to: 'orders#update'

  get '/tables', to: 'tables#get'
end
